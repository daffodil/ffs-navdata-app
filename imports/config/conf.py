# -*- coding: utf-8 -*-

# Author: <Peter Morgan> pete at freeflightsim dot org

# Load basic configuration

import sys
import os
import json


class Config:
	
	def __init__(self):
		
		self.data_root = None
		self.temp_dir = None

	def __repr__(self):
		return "<ConfigObject data_root:'%s' temp_dir:'%s'>" % (self.data_root, self.temp_dir)

	def set_data_root(self, path):
		if os.path.exists(path):
			#TODO chack writable etc
			self.data_root = path
			return
		raise Exception("DataPath '%s' does not exist" % path)

	def set_temp_dir(self, path):
		if os.path.exists(path):
			#TODO chack writable etc
			self.temp_dir = path
			return
		raise Exception("TempPath '%s' does not exist" % path)
		
		
	def temp_file(self, file_name):
		return open(self.temp_dir + file_name, "r")

def load():

	config_file = os.path.abspath(os.path.dirname(__file__) + '/../../etc/config.json')
	config = json.loads(open(config_file, 'r').read())
	
	C = Config()
	C.set_data_root(config['data_root'])
	C.set_temp_dir(config['temp_dir'])
	return C